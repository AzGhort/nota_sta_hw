local sensorInfo = {
	name = "CheckUnitsStates",
	desc = "Checks states of units, and assignes them a state (eventually a plan)",
	author = "AzGhort",
	date = "2019-05-12"
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(plans, unitsStates)
    local newStates = {}
    for unit, metastate in pairs(unitsStates["states"]) do
        local state = metastate.state
        local newState = {}
        if state == "idle" then
            -- assign first unassigned plan, if there is one
            if plans.firstUnassigned <= #plans.targets then
                local target = plans["targets"][plans.firstUnassigned]
                newState["targetID"] = plans["ids"][plans.firstUnassigned]
                plans.firstUnassigned = plans.firstUnassigned + 1
                local plan = plans.plans[target]["toTarget"]
                newState["state"] = "toTarget"
                newState["plan"] = plan
                newState["planIndex"] = 1
            else 
                newState["state"] = "idle"
            end
        elseif state == "toTarget" then
            -- switch to loading
            -- we need to persist the plan and targetID
            newState["state"] = "loading"
            newState["plan"] = metastate.plan
            newState["targetID"] = metastate["targetID"]
        elseif state == "loading" then
            -- switch to way to base
            local target = metastate.plan[#metastate.plan]
            local plan = plans.plans[target]["toBase"]
            newState["state"] = "toBase"
            newState["plan"] = plan
            newState["planIndex"] = 1
            newState["targetID"] = metastate["targetID"]
        elseif state == "toBase" then
            -- switch to waiting (inertia bug)
            newState["state"] = "waiting"
            newState["waitStart"] = false
            newState["targetID"] = metastate["targetID"]
        elseif state == "waiting" then
            -- switch to unloading
            newState["state"] = "unloading"
            newState["targetID"] = metastate["targetID"]
            newState["evPoint"] = plans["evPoints"][1]
        elseif state == "unloading" then
            -- switch to moveAside
            newState["state"] = "moveAside"
            newState["evPoint"] = plans["evPoints"][1]
        elseif state == "moveAside" then
            newState["state"] = "idle"
        elseif state == "dead" then
            newState["state"] = "dead"
        end
        newStates[unit] = newState
    end
    local meta = {}
    meta["states"] = newStates
    meta["finishedUnits"] = 0
    return meta
end