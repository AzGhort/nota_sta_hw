local sensorInfo = {
	name = "AssignPlans",
	desc = "Assigns plans to units, given a list of plans and their names",
	author = "AzGhort",
	date = "2019-05-12"
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(plans, planName)
    local assigned = {}
    local i = 1
    for target, plans in pairs(plans) do
        if i > #units then
            break
        end
        local currentPlan = {}
        currentPlan["plan"] = plans[planName]
        currentPlan["pathIndex"] = 1
        assigned[units[i]] = currentPlan
        i = i + 1
    end
    return assigned
end