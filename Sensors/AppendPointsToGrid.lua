local sensorInfo = {
	name = "AppendPointsToGrid",
	desc = "Appends points to grid, so that the new points in grid have exactly one neighbour, the closest point from grid.",
	author = "AzGhort",
	date = "2019-05-09"
}

return function(grid, points)
    new = {}
    for _, newpoint in ipairs(points) do
        closest = nil
        neigbs = {}
        minDist = math.huge
        for position, neighbours in pairs(grid) do
            dist = position:Distance(newpoint)
            if dist < minDist then
                closest = position
                neigbs = neighbours
                minDist = dist
            end
        end
        -- new point to add
        new[newpoint] = { closest }
        -- edit the closest point in the grid
        neigbs[#neigbs + 1] = newpoint
        grid[closest] = neigbs
    end
    for point, ns in pairs(new) do
        grid[point] = ns
    end
    return grid
end