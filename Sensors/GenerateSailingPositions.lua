local sensorInfo = {
	name = "GenerateSailingPositions",
	desc = "Generates positions for sailing bots following commander in circles around him",
	author = "AzGhort",
	date = "2019-05-09"
}

return function(com)
    local positions = {}
    local radius = 100
    local quarter = radius/math.sqrt(2)
    local circles = (#units / 8) + 1
    local y = com.y
    local z = com.z
    local x = com.x
    for i=1, circles do
        -- halfs
        positions[#positions + 1] = Vec3(x + i*radius, y, z)
        positions[#positions + 1] = Vec3(x, y, z - i*radius)
        positions[#positions + 1] = Vec3(x - i*radius, y, z)
        positions[#positions + 1] = Vec3(x, y, z + i*radius)
        -- quarters
        positions[#positions + 1] = Vec3(x + i*quarter, y, z + i*quarter)
        positions[#positions + 1] = Vec3(x + i*quarter, y, z - i*quarter)
        positions[#positions + 1] = Vec3(x - i*quarter, y, z + i*quarter)
        positions[#positions + 1] = Vec3(x - i*quarter, y, z - i*quarter)
    end
    return positions
end