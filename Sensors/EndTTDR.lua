VFS.Include("modules.lua") 
VFS.Include(modules.attach.data.path .. modules.attach.data.head)
attach.Module(modules, "message") 

function getInfo()
    return {
        period = 0 
    }
end

return function()
    message.SendRules({
        subject = "manualMissionEnd",
        data = {},
    })
end