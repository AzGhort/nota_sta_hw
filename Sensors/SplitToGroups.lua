local sensorInfo = {
	name = "SplitToGroups",
	desc = "Splits units to groups containing exactly one unit.",
	author = "AzGhort",
	date = "2019-04-23"
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

-- @description return groups of units
return function()
    local unitsGroups = {}
    for i = 1,#units do
        unitsGroups[i] = units[i]
    end
    return unitsGroups
end
