local sensorInfo = {
	name = "InitUnitStates",
	desc = "Initialize units states for transport scheduling.",
	author = "AzGhort",
	date = "2019-05-09"
}

VFS.Include("modules.lua")
VFS.Include(modules.attach.data.path .. modules.attach.data.head) 
attach.Module(modules, "message")

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()
    local states = {}
    local meta = {}
    for i = 1, #units do
        local schedule = {}
        schedule["state"] = "idle"
        states[units[i]] = schedule
    end
    meta["states"] = states
    meta["finishedUnits"] = 0
    return meta
end
