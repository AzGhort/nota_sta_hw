local sensorInfo = {
	name = "CommanderPosition",
	desc = "Sends data about the commander's position",
	author = "Simon Stachura",
	date = "2019-04-23",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	if #units > 0 then
		local ID = units[1]
		local x,y,z = Spring.GetUnitPosition(ID)
		return Vec3(x, y, z)
	end
end


-- for unitID in units do
--	local unitDefID = Spring.GetUnitDefID(unitID)
--	local name = UnitDefs[unitDefID].name
--	if name == "armbcom" then x,y,z = Spring.GetUnitPosition(unitID)
--		end
--	end
