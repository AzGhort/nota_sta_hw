local sensorInfo = {
	name = "CreateGrid",
	desc = "Creates grid of points, mapping points to neighbours",
	author = "AzGhort",
	date = "2018-05-13",
}

return function(pointSamples, range)
	local points = {}
	local dist

	for i = 1, #pointSamples, 1 do
		local neighbours = {}
		for j = 1, #pointSamples, 1 do
			dist = -1
			if i ~= j then
				dist = pointSamples[i]:Distance(pointSamples[j])
			end
			if dist ~= -1 and dist <= range then
				neighbours[#neighbours + 1] = pointSamples[j]
			end
		end
		points[pointSamples[i]] = {}
		points[pointSamples[i]]["neighbours"] = neighbours
		points[pointSamples[i]]["previous"] = nil
	end

	return points
end