local sensorInfo = {
	name = "BoxesOfDeathPositions",
	desc = "Sends data about the boxes-of-death's positions",
	author = "AzGhort",
	date = "2019-05-09",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
    names = {}
	if #units > 0 then
		local ID = units[1]
		local x,y,z = Spring.GetUnitPosition(ID)
		local unitDefID = Spring.GetUnitDefID(unitID)
		local name = UnitDefs[unitDefID].name
		names[#names + 1] = name
    end
    return names
end