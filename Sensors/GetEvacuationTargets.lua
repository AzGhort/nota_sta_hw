local sensorInfo = {
	name = "GetEvacuationTargets",
	desc = "Returns position of all evacuatable units in ttdr mission",
	author = "AzGhort",
	date = "2019-05-16"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

VFS.Include("modules.lua")
VFS.Include(modules.attach.data.path .. modules.attach.data.head)
attach.Module(modules, "message") 


function addToTargets(targets, positions, ids)
    for i=1, #targets["positions"] do
        positions[#positions + 1] = targets["positions"][i]
        ids[#ids + 1] = targets["ids"][i] 
    end
end

function generateEvacuationBasePoints(targets, startPoints, numInRow)
    local basePoints = {}
    local base = startPoints["base"]
    local radius = startPoints["baseRadius"]
    local aHalf = (1/math.sqrt(2))*radius
    local cornerAX = base.x - aHalf
    local cornerAZ = base.z - aHalf
    local cornerCX = base.x + aHalf
    local cornerCZ = base.z + aHalf
    local dz = 2*aHalf / (numInRow - 1)
    local dx = dz 
    for x=cornerAX, cornerCX, dx do
        for z=cornerAZ, cornerCZ, dz do 
            basePoints[#basePoints + 1] = Vec3(x, Spring.GetGroundHeight(x, z), z)
        end
    end
    return basePoints
end

return function(startPoints)
    local targets = {}
    local ids = {}
    local positions = {}
    local boxes = Sensors.nota_sta_hw.UnitsPositions("armbox")
    local hatracks = Sensors.nota_sta_hw.UnitsPositions("armmllt")
    local hammers = Sensors.nota_sta_hw.UnitsPositions("armham")
    local bulldogs = Sensors.nota_sta_hw.UnitsPositions("armbull")
    local rockos = Sensors.nota_sta_hw.UnitsPositions("armrock")
    addToTargets(boxes, positions, ids)
    addToTargets(hatracks, positions, ids)
    addToTargets(hammers, positions, ids)
    addToTargets(bulldogs, positions, ids)
    addToTargets(rockos, positions, ids)
    targets["positions"] = positions
    targets["ids"] = ids
    targets["basePositions"] = generateEvacuationBasePoints(targets, startPoints, 6)
    targets["evPoints"] = startPoints["evPoints"]
    return targets
end