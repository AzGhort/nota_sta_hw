local sensorInfo = {
	name = "InitializeStartPoints",
	desc = "Initializes a start point table",
	author = "AzGhort",
	date = "2019-05-16"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(point1, point2)
    local starts = {}
    local trueBase = Sensors.core.missionInfo().safeArea["center"]
    local evPoints = {}
    evPoints[1] = point1
    evPoints[2] = point2
    starts["base"] = trueBase
    starts["baseRadius"] = Sensors.core.missionInfo().safeArea["radius"]
    starts["evPoints"] = evPoints
    return starts
end