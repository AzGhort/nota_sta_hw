local sensorInfo = {
	name = "FindClosestPointsToTargets",
	desc = "Finds closest points to targets in a list of points.",
	author = "AzGhort",
	date = "2019-05-09"
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(points, targets)
    local targetPoints = {}
    for i, target in ipairs(targets) do
        local closest = nil
        local minDist = math.huge
        for j=1, #points do
            local dist = target:Distance(points[j])
            if dist < minDist then
                closest = points[j]
                minDist = dist
            end
        end
        targetPoints[#targetPoints + 1] = closest
    end
    return targetPoints
end