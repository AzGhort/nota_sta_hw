local sensorInfo = {
	name = "ClosestNPointsToPosition",
	desc = "Finds N closest points to the given position.",
	author = "AzGhort",
	date = "2019-05-09"
}

-- points is a list of points, area is a Vec3
return function(points, area, n)
    closest = {}
    current_closest = 0
    for point in points do
        if current_closest < n then
            closest[#closest+1] = point
            closest = closest + 1
        else
            for i,clos in ipairs(closest) do
                distOld = clos:Distance(area)
                distNew = point:Distance(area)
                if distNew < distOld then
                    closest[i] = point
                    break
                end
            end
        end
    end
    return closest
end