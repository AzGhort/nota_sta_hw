local sensorInfo = {
	name = "HillPoints",
	desc = "Conquers all hill-points.",
	author = "AzGhort",
	date = "2019-04-23"
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return hills points
return function(tolerance)
	local points = {}
	local height = 191

	-- find all points higher than 191
	for i=1,Game.mapSizeX,100 do
		for j=1,Game.mapSizeZ,100 do
			if (Spring.GetGroundHeight(i,j) > height - 1) then
				points[#points + 1] = Vec3(i, height, j)
			end
		end
	end

	local pruned = { }
	-- prune points from same hills (euclidean distance less than tolerance argument)
	for i,point in ipairs(points) do
		local unique = true
		for j,point2 in ipairs(pruned) do
			local dist = point:Distance(point2)
			if dist < tolerance then
				unique = false
				break
			end
		end
		if (unique) then
			pruned[#pruned + 1] = points[i]
		end
  end

	return pruned
end
