local sensorInfo = {
	name = "InitializePaths",
	desc = "Prepare a table of predecessors",
	author = "AzGhort",
	date = "2019-05-15"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(points)
    paths = {}
    for i=1, #points do
        paths[points[i]] = nil
    end
    return paths
end