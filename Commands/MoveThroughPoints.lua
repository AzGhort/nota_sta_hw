function getInfo() 
    return
    {
	    onNoUnits = SUCCESS, -- instant success
		tooltip = "Move selected units to target, moving in a greedy way only on points given",
        parameterDefs =
        {
            { 
				name = "points",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "threshold",
				variableType = "number",
				componentType = "editBox",
				defaultValue = "",
            }
        }
    }
end