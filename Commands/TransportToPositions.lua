function getInfo() 
    return
    {
	    onNoUnits = SUCCESS, -- instant success
		tooltip = "Transports given units to positions",
        parameterDefs =
        {
            { 
				name = "tranportedUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "targetPositions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "unitsStates",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = {}
            }
        }
    }
end

local states = {}

function AssignUnitsTargets(units, transported, positions, states)
    for i=1,#units do
        local plan = {}
        local transportedIndex = i
        if i > #transported then
            transportedIndex = 1
        end
        local positionsIndex = i
        if i > #positions then
            positionsIndex = 1
        end
        plan["state"] = "toTarget"
        plan["targetID"] = transported[transportedIndex]
        plan["targetPosition"] = positions[positionsIndex]
        states[units[i]] = plan
    end
end

function Run(self, units, parameter)
    local transported = parameter.tranportedUnits
    local targets = parameter.targetPositions
    local states = parameter.unitsStates
    local plans
    if #states == 0 then
        AssignUnitsTargets(units, transported, positions, states)
    end

    return RUNNING
end