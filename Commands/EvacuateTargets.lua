function getInfo() 
    return
    {
	    onNoUnits = SUCCESS, -- instant success
		tooltip = "Evacuate targets according to states assigned to units",
        parameterDefs =
        {
            { 
				name = "unitsStates",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            }
        }
    }
end

local THRESHOLD = 185

function Move(unitID, metastate, finished)
    if metastate.planIndex == #metastate.plan + 1 then
        finished["val"] = finished["val"] + 1
        return
    end
    Spring.GiveOrderToUnit(unitID, CMD.MOVE, metastate.plan[metastate.planIndex]:AsSpringVector(), {})
    local position = Vec3(Spring.GetUnitPosition(unitID))
    -- no tolerance on base, to avoid overlapping unload
    if (metastate.state == "toBase" and metastate.planIndex == #metastate.plan
        and position:Distance(metastate.plan[metastate.planIndex]) < 1) then
        metastate.planIndex = metastate.planIndex + 1
    -- arrived to next waypoint
    else
        if (position:Distance(metastate.plan[metastate.planIndex]) < THRESHOLD) then
            metastate.planIndex = metastate.planIndex + 1
        end
    end
end

function Load(unitID, metastate, finished)
    if (Spring.GetUnitTransporter(metastate.targetID) == unitID) or (not Spring.ValidUnitID(metastate.targetID)) then
        finished["val"] = finished["val"] + 1
    else
        Spring.GiveOrderToUnit(unitID, CMD.LOAD_UNITS, {metastate.targetID}, {})
    end
end

function Unload(unitID, metastate, finished)
    if (Spring.GetUnitTransporter(metastate.targetID) == nil) or (not Spring.ValidUnitID(metastate.targetID)) then
        finished["val"] = finished["val"] + 1
        Spring.GiveOrderToUnit(unitID, CMD.MOVE, metastate.evPoint:AsSpringVector(), {})
    else
        local position = Vec3(Spring.GetUnitPosition(unitID))
        Spring.GiveOrderToUnit(unitID, CMD.UNLOAD_UNIT, position:AsSpringVector(), {})
    end
end

function MoveAside(unitID, metastate, finished)
    Spring.GiveOrderToUnit(unitID, CMD.MOVE, metastate.evPoint:AsSpringVector(), {})
    local position = Vec3(Spring.GetUnitPosition(unitID))
    if (position:Distance(metastate.evPoint) < THRESHOLD) then
        finished["val"] = finished["val"] + 1
    end
end

function Wait(unitID, metastate, finished)
    if not(metastate.waitStart) then
        Spring.GiveOrderToUnit(unitID, CMD.TIMEWAIT, {10}, {})
        metastate.waitStart = true
        metastate.endTime = Spring.GetGameSeconds() + 12
    elseif Spring.GetGameSeconds() > metastate.endTime then
        finished["val"] = finished["val"] + 1
    end
    end

function Run(self, units, parameter)
    local counter = 0
    -- hack to force pass by reference
    local finished = {}
    finished["val"] = 0
    for unitID, metastate in pairs(parameter.unitsStates["states"]) do
        -- according to API, only returns true for a short time, then nil
        local recentlyDied = not Spring.ValidUnitID(unitID)
        if recentlyDied or recentlyDied == nil then
            metastate.state = "dead"
            finished["val"] = finished["val"] + 1
        elseif metastate.state == "toTarget" or metastate.state == "toBase" then
            Move(unitID, metastate, finished)
        elseif metastate.state == "loading" then
            Load(unitID, metastate, finished)
        elseif metastate.state == "waiting" then
            Wait(unitID, metastate, finished)
        elseif metastate.state == "unloading" then
            Unload(unitID, metastate, finished)
        elseif metastate.state == "moveAside" then
            MoveAside(unitID, metastate, finished)
        elseif metastate.state == "idle" or metastate.state == "dead" then
            finished["val"] = finished["val"] + 1
        end
        counter = counter + 1
    end
    parameter.unitsStates["finishedUnits"] = finished
    if counter == finished["val"] then
        return SUCCESS
    else
        return RUNNING
    end
end
